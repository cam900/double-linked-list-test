/*
    Double Linked List demo
*/

template<class T>
struct Node
{
    T data;
    Node<T>* prev; // 현재 노드의 이전 노드
    Node<T>* next; // 현재 노드의 다음 노드
};

template<class T>
class DoubleLinkedList
{
private:
    unsigned int count = 0; // 노드의 개수
    Node<T>* header;
    Node<T>* trailer;
public:
    // 생성자
    DoubleLinkedList()
        : count(0)
    {
        header = new Node<T>{ 0, NULL, NULL };
        trailer = new Node<T>{ 0, NULL, NULL };
        header->next = trailer;
        trailer->prev = header;
    }

    // 소멸자
    ~DoubleLinkedList()
    {
        while (!empty())
        {
            pop_front();
        }
        delete header;
        delete trailer;
    }

    // 지정한 노드(p)의 앞에 val값을 가진 노드 삽입
    Node<T>* insert(Node<T>* p, T val)
    {
        // 삽입할 노드 생성
        Node<T>* new_node = new Node<T>{ val, p->prev, p };
        new_node->prev->next = new_node;
        new_node->next->prev = new_node;
        count++;
        return new_node;
    }

    // 맨앞에 val값을 가진 노드 삽입하기
    Node<T>* push_front(T val)
    {
        return insert(header->next, val);
    }

    // 맨뒤에 val값을 가진 노드 삽입하기
    Node<T>* push_back(T val)
    {
        return insert(trailer, val);
    }

    // 맨앞
    Node<T>* head()
    {
        return header->next;
    }

    // 맨뒤
    Node<T>* tail()
    {
        return trailer->prev;
    }

    // 노드의 개수
    inline unsigned int size() const
    {
        return count;
    }

    // 데이터가 있는지 체크
    inline bool empty() const
    {
        return count == 0;
    }

    // 지정하는 노드(p)의 삭제
    DoubleLinkedList& erase(Node<T>* p)
    {
        if ((!empty()) && (p != header) && (p != trailer))
        {
            p->prev->next = p->next;
            p->next->prev = p->prev;
            delete p;
            count--;
        }
        return *this;
    }

    // 맨앞에 있는 노드 삭제
    DoubleLinkedList& pop_front()
    {
        if (!empty())
            return erase(header->next);
        return *this;
    }

    // 맨뒤에 있는 노드 삭제
    DoubleLinkedList& pop_back()
    {
        if (!empty())
            return erase(trailer->prev);
        return *this;
    }

    // 순서대로 노드에 있는 데이터 출력
    DoubleLinkedList& print_all()
    {
        Node<T>* curr = header->next;
        while (curr != trailer)
        {
            std::cout << curr->data << ", ";
            curr = curr->next;
        }
        std::cout << std::endl;
        return *this;
    }

    // 역순으로 노드에 있는 데이터 출력
    DoubleLinkedList& print_all_reverse()
    {
        Node<T>* curr = trailer->prev;
        while (curr != header)
        {
            std::cout << curr->data << ", ";
            curr = curr->prev;
        }
        std::cout << std::endl;
        return *this;
    }
};

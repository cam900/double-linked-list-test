﻿/*
    Double Linked List test source
*/

#include <iostream>
#include "DoubleLinkedList.hpp"

int main()
{
    DoubleLinkedList<int> dllist;
    dllist.push_back(10);
    dllist.push_back(20);
    dllist.push_back(30);
    dllist.print_all();
    dllist.print_all_reverse();

    dllist.pop_front(); // 10제거
    dllist.pop_back();  // 30제거
    dllist.print_all();

    dllist.push_front(100); // 20의 앞에 100 삽입
    dllist.push_back(300);  // 20의 뒤에 300 삽입
    dllist.print_all_reverse();
    dllist.print_all();
}
